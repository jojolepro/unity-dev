﻿/*
 * 
 * Copyright Joël Lupien (Jojolepro) 2017
 * All right reserved
 * 
 * 
 */


using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class ColliderTriggerCallback : UnityEvent<Collider>
{
}

public class ColliderTrigger : MonoBehaviour {
	public LayerMask layers;
	public ColliderTriggerCallback onEnter;
	public ColliderTriggerCallback onStay;
	public ColliderTriggerCallback onExit;
	void OnTriggerEnter(Collider other) {
		if(!layers.IsIncluded(other.gameObject.layer))
			return;
		onEnter.Invoke(other);
	}
	void OnTriggerStay(Collider other){
		if(!layers.IsIncluded(other.gameObject.layer))
			return;
		onStay.Invoke(other);
	}
	void OnTriggerExit(Collider other) {
		if(!layers.IsIncluded(other.gameObject.layer))
			return;
		onExit.Invoke(other);
	}
}
