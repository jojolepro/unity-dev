﻿using UnityEngine;
using System.Collections;

//Naming relative to client
public class NetworkPackets {
    public const byte Controller = 0;
    public const byte Update = 1;
    //new
    public const byte GameState = 0;
    public const byte EntityManaging = 1;
    public const byte EntityUpdate = 2;
    public const byte Chat = 3;
    public const byte Input = 4;
    public class ControllerSubjects
    {
        public const ushort GetLocalPlayerRequest = 0;
        public const ushort SpawnPlayer = 1;
    }
    public class UpdateSubjects
    {
        public const ushort SetPos = 0;
    }

    //new
    public class SGameState
    {
        public const ushort ChangeMap = 0;
        public const ushort EndTime = 1;
        public const ushort GameState = 2;//map,remaining time,player count

    }
    public class SEntityManaging
    {
        public const ushort AddPlayer = 0;
        public const ushort RemovePlayer = 1;
    }
    public class SEntityUpdate
    {
        public const ushort UpdateTransform = 0;
    }
    public class SChat
    {
        public const ushort AddChatEntry = 0;
        public const ushort SendChatMsg = 1;
    }
    public class SInput
    {
        public const ushort SendInput = 0;
    }
}
